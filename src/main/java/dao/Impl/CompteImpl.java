package dao.Impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import dao.EntityRepository.CompteRepository;
import dao.entities.Compte;

public class CompteImpl implements CompteRepository {
	EntityManagerFactory emf = null; 
	EntityManager entitymanager= null; 
	EntityTransaction entitytrsaction = null; 
	public CompteImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	public Compte ajouter(Compte t) {
		entitymanager= emf.createEntityManager();
		entitytrsaction = entitymanager.getTransaction();
		try {
			entitytrsaction.begin();
			entitymanager.persist(t);
			entitytrsaction.commit();
			
		} catch (Exception e) {
			entitytrsaction.rollback();
			e.printStackTrace();
		}
		
		return t;
	}

	public Compte modifier(Compte t) {
		// TODO Auto-generated method stub
		return null;
	}

	public void supprimer(Compte t) {
		// TODO Auto-generated method stub

	}

	public Compte rechercherByMc(String mc) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Compte> rechercherTous() {
		// TODO Auto-generated method stub
		return null;
	}

}
