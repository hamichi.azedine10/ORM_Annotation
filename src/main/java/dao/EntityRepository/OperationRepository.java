package dao.EntityRepository;

import dao.entities.Operation;

public interface OperationRepository extends EntityRepository<Operation> {

}
