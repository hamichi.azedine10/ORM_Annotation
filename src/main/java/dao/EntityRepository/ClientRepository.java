package dao.EntityRepository;

import dao.entities.Client;

public interface ClientRepository extends EntityRepository<Client> {
	public Client rechercherClientById  (Long id);
}
