package dao.EntityRepository;

import java.util.List;

public interface EntityRepository <T>{
   public T ajouter(T t);
   public T modifier(T t );
   public void supprimer (T t );
   public T  rechercherByMc(String mc);
   public List<T> rechercherTous();
	
}
