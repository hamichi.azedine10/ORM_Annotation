package dao.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "T_Operation")
public class Operation {
	@Id
	@GeneratedValue
	private Long numeroOperation;
	@Temporal(TemporalType.DATE)
	private Date dateOperation ;
	private double versement;
	private double retrait;
	@ManyToOne
	private Compte compte ;

	public Operation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Operation(double versement, double retrait, Compte compte) {
		super();
		this.versement = versement;
		this.retrait = retrait;
		this.compte = compte;
	}
	public Long getNumeroOperation() {
		return numeroOperation;
	}
	public void setNumeroOperation(Long numeroOperation) {
		this.numeroOperation = numeroOperation;
	}
	public Date getDateOperation() {
		return dateOperation;
	}
	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}
	public double getVersement() {
		return versement;
	}
	public void setVersement(double versement) {
		this.versement = versement;
	}
	public double getRetrait() {
		return retrait;
	}
	public void setRetrait(double retrait) {
		this.retrait = retrait;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	@Override
	public String toString() {
		return "Operation [numeroOperation=" + numeroOperation + ", dateOperation=" + dateOperation + ", versement="
				+ versement + ", retrait=" + retrait + ", compte=" + compte + "]";
	}
	

}
