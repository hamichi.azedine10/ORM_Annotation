package dao.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Compte Epargne")
public class CompteEpargne extends Compte{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


@Override
	public String toString() {
		return "CompteEpargne [taux=" + taux + ", numeroCompte=" + numeroCompte + ", dateCreation=" + dateCreation
				+ ", solde=" + solde + "]";
	}
private double taux; 
 

public CompteEpargne(Long numeroCompte,Client client,  double solde,double taux) {
		super();
		super.dateCreation=new Date();
		this.taux = taux;
	}
public CompteEpargne() {
	super();
}


}
