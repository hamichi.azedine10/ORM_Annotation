package dao.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_CLIENT")
public class Client implements Serializable{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


public Client(String nomClient, String adresse) {
		super();
		this.nomClient = nomClient;
		this.adresse = adresse;
	}
 @Id
 // generer la cle incrimentale 
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 private Long codeClient ; 
 private String  nomClient ; 
 private String adresse ;
 
 @OneToMany(mappedBy = "client",fetch = FetchType.LAZY)
 private Set<Compte> comptes = new HashSet<Compte>() ; 
 
 
 public Long getCodeClient() {
	return codeClient;
}
public void setCodeClient(Long codeClient) {
	this.codeClient = codeClient;
}
public String getNomClient() {
	return nomClient;
}
public void setNomClient(String nomClient) {
	this.nomClient = nomClient;
}
public String getAdresse() {
	return adresse;
}
public void setAdresse(String adresse) {
	this.adresse = adresse;
}
public Set<Compte> getComptes() {
	return comptes;
}
public void setComptes(Set<Compte> comptes) {
	this.comptes = comptes;
}
public void ajoutercompte (Compte compte) {
	 comptes.add(compte);
	 compte.setClient(this);
};

@Override
public String toString() {
	return "Client [codeClient=" + codeClient + ", nomClient=" + nomClient + ", adresse=" + adresse + "]";
}

}
