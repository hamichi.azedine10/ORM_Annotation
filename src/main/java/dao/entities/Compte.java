package dao.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//une classe pour les deux comptes 

@Entity
@Table(name = "T_COMPTES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// definir la colonne discriminante 
@DiscriminatorColumn(name = "TypeCompte", discriminatorType = DiscriminatorType.STRING)
public abstract class Compte implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@Id 
@GeneratedValue(strategy = GenerationType.IDENTITY)
protected Long numeroCompte; 

@Temporal(TemporalType.TIMESTAMP) 
protected Date dateCreation;

protected double solde;
@ManyToOne
protected  Client client ;

// colonne technique  juste pour la lecture 
@Column(name = "discriminant", insertable = false, updatable = false)
protected String     discriminant;

@OneToMany (mappedBy = "compte",fetch = FetchType.LAZY)
private Set<Operation> operations=new HashSet<Operation>();
 
 public void ajouterOperation (Operation operation) {
	 operations.add(operation);
	 operation.setCompte(this);
 }

public Compte(double solde, Client client) {
	super();
	this.dateCreation = new Date();
	this.solde = solde;
	this.client = client;
	
}

public Compte() {
	super();
}


public Long getNumeroCompte() {
	return numeroCompte;
}

public void setNumeroCompte(Long numeroCompte) {
	this.numeroCompte = numeroCompte;
}

public Date getDateCreation() {
	return dateCreation;
}

public void setDateCreation(Date dateCreation) {
	this.dateCreation = dateCreation;
}

public double getSolde() {
	return solde;
}

public void setSolde(double solde) {
	this.solde = solde;
}

public Client getClient() {
	return client;
}

public void setClient(Client client) {
	this.client = client;
}

public Set<Operation> getOperations() {
	return operations;
}

public void setOperations(Set<Operation> operations) {
	this.operations = operations;
}


public String getDiscriminant() {
	return discriminant;
}


@Override
public String toString() {
	return "Compte [numeroCompte=" + numeroCompte + ", dateCreation=" + dateCreation + ", solde=" + solde + ", client="
			+ client + "]";
}



}
