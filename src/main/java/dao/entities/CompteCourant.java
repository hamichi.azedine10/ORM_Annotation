package dao.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Compte Courant")
public class CompteCourant extends Compte {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String toString() {
		return "CempteCourant [decouvert=" + decouvert + ", numeroCompte=" + numeroCompte + ", dateCreation="
				+ dateCreation + ", solde=" + solde +  "]";
	}

	public CompteCourant() {
		super();
	}

public CompteCourant(Long numeroCompte,Client client,  double solde,double decouvert) {
		super();
		super.dateCreation=new Date();
		this.decouvert = decouvert;
		super.setSolde(solde);
	}
private double decouvert;
public double getDecouvert() {
	return decouvert;
}

public void setDecouvert(double decouvert) {
	this.decouvert = decouvert;
}


}
