package dao;

import dao.Impl.ClientImpl;
import dao.Impl.CompteImpl;
import dao.entities.Client;
import dao.entities.CompteCourant;
import dao.entities.CompteEpargne;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class daoImpl {
	public static void main(String[] args) {
	
		EntityManagerFactory emf= Persistence.createEntityManagerFactory("JPA_mapping"); 
		Client cl1 = new Client("azedine ", "5 rue des champs ");
		
		CompteEpargne compte1 = new CompteEpargne(null, cl1, 0, 0);
		CompteCourant comptecourant = new CompteCourant(null, cl1,2000,-550);
		ClientImpl clientimplimentation = new ClientImpl(emf);
		CompteImpl compteimpl = new CompteImpl(emf);
		compteimpl.ajouter(compte1);
		compteimpl.ajouter(comptecourant);
		System.out.println("okok");
		clientimplimentation.ajouter(cl1);
		// tp non terminé 
	}

}
